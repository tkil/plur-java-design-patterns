package com.tkil.patterns;

public class BuilderDemo {
    public static void main(String args[]) {
        BuilderExample.Builder builder = new BuilderExample.Builder();
        builder.bread("Wheat").condiments("Lettus").dressing("Mayo").meat("Turkey");

        BuilderExample lunchOrder = builder.build();

        System.out.println(lunchOrder.getBread());
        System.out.println(lunchOrder.getCondiments());
        System.out.println(lunchOrder.getDressing());
        System.out.println(lunchOrder.getMeat());
    }
}
